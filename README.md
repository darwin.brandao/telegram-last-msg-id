# telegram-last-msg-id

Very simple script to get the last message ID from a public Telegram channel.

It uses:

- cURL
- xmllint
- grep

## Usage

You must provide a valid Telegram Channel URL as argument.

```
./telegram-last-msg-id.sh TELEGRAM_CHANNEL_URL
```

## How it works

1. It uses __cURL__ to get HTML from "Preview channel"'s URL (the same link underneath the "View in Telegram" button in the channel's URL)
2. It uses __xmllint__ to extract the messages using XPath (by selecting @data-post attributes and getting the last entry)
3. It gets the attribute value by using a Regex pattern along with __grep__

## License

MIT License