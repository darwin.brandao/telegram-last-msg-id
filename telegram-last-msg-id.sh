#!/bin/bash

# Check if Channel's URL was passed in as argument
if [ $# -eq 0 ]; then
    echo "No telegram channel provided."
    echo "Please pass the channel's URL as argument."
    echo "Example: https://t.me/blogdarwinbrandao"
    exit 1
fi

# Convert from regular channel's url to the "chat preview" url
url="$(echo "$1" | awk '{gsub(/https:\/\/t.me/, "https://t.me/s", $0); print}')"

# Make request, use XPath to find last message and grep the ID from it
curl -s $url | xmllint --html --xpath 'string((//@data-post)[last()])' - 2>/dev/null | grep -Eo '[0-9]{1,5}'
